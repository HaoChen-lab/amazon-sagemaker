# Get Started with Amazon SageMaker Notebook Instances

## Introduction

One of the best ways for machine learning (ML) practitioners to use Amazon SageMaker is to train and deploy ML models using SageMaker notebook instances. The SageMaker notebook instances help create the environment by initiating Jupyter servers on Amazon Elastic Compute Cloud (Amazon EC2) and providing preconfigured kernels with the following packages: the Amazon SageMaker Python SDK, AWS SDK for Python (Boto3), AWS Command Line Interface (AWS CLI), Conda, Pandas, deep learning framework libraries, and other libraries for data science and machine learning.

## Use Case

This tutorial explains how to launch an Amazon SageMaker Notebook instance. We go through all the steps you need to set up lifecycle configurations and using Git and VPC attachments in Amazon SageMaker so you can onboard quickly. 

## Overview

**Topics**
- Step 1: Create a lifecycle configuration
- Step 2: Add a repository
- Step 3: Create a Notebook instance
- Step 4: Clean up

## Step by Step

#### Step 1: Create a lifecycle configuration

A lifecycle configuration provides shell scripts that run only when you create the notebook instance or whenever you start one. When you create a notebook instance, you can create a new lifecycle configuration and the scripts it uses or apply one that you already have.

1. Open the Amazon SageMaker console at https://console.aws.amazon.com/sagemaker.
2. Choose **Lifecycle configurations**, and then choose **Create Lifecycle configurations**.
3. On the **Create Lifecycle configurations** page, provide the following information:
    - For **Lifecycle configurations** name, type a name for your Lifecycle configurations.
    - (Optional) To create a script that runs when you create the notebook and every time you start it,
    choose **Start notebook**. In the **Start notebook editor**, type the script.
    - (Optional) To create a script that runs only once, when you create the notebook, choose **Create notebook**. In the **Create notebook** editor, type the script configure networking.
    - Choose **Create configuration** .

For example, if you want to install a package only for the **python3 environment**, use the following script code:

``` JSON
#!/bin/bash
sudo -u ec2-user -i <<'EOF'
# This will affect only the Jupyter kernel called "conda_python3".
source activate python3
# Replace myPackage with the name of the package you want to install.
pip install myPackage
# You can also perform "conda install" here as well.
source deactivate
EOF
```
If you want to **attach to your Github**, use the following script code:

```JSON
#!/bin/bash

set -e

#OVERVIEW
#This script sets username and email adress in Git config

#PARAMETERS
YOUR_USER_NAME="*****"
YOUR_EMAIL_ADDRESS="*****"

sudo -u ec2-user -i <<EOF

git config --global user.name "$*****"
git config --global user.email "$*****"

EOF

```
#### Step 2: Add a repository

To manage your GitHub repositories, easily associate them with your notebook instances, and associate credentials for repositories that require authentication, add the repositories as resources in your Amazon SageMaker account. You can view a list of repositories that are stored in your account and details about each repository in the SageMaker console and by using the API.

**To add a Git repository as a resource in your SageMaker account**

1. Open the SageMaker console at https://console.aws.amazon.com/sagemaker/.
2. Choose Git repositories, then choose **Add repository**. For the folliowing page, there are two options.

**➤AWS CodeCommit**
- To add an CodeCommit repository, choose **AWS CodeCommit**. 
- To create a **new CodeCommit repository**, choose **Create new repository**.
( To add an existing CodeCommit repository,  choose a repository from the list.)
- Enter a name for the repository that you can use in both CodeCommit and SageMaker.
- Choose **Create repository**.    

**➤GitHub/Other Git-based repo**
- Choose **GitHub/Other Git-based repo**.
- Enter a name of up to 63 characters.
- Enter the URL for the repository. Do not provide a user name in the URL. Add the username and password in AWS Secrets Manager as described in the next step.
- For **Git credentials**, choose the credentials to use to authenticate to the repository. This is necessary only if the Git repository is private.
    - To use an existing AWS Secrets Manager secret, choose **Use existing secret**, and then choose a secret from the list. 
    - To create a new AWS Secrets Manager secret, choose **Create secret**, enter a name for the secret, and then enter the username and password to use to authenticate to the repository. 
    
#### Step 3: Create a Notebook instance
1. Open the Amazon SageMaker console at https://console.aws.amazon.com/sagemaker/.
2. Choose **Notebook instances**, and then choose **Create notebook instance**.
3. On the **Create notebook instance page**, provide the following information:  
    - a. For **Notebook instance name**, type a name for your notebook instance.
    - b. For **Instance type**, choose ml.t2.medium. This is the least expensive instance type that notebook instances support, and it suffices for this exercise. If a ml.t2.medium instance type isn't available in your current AWS Region, choose ml.t3.medium.
    - c. For **IAM role**, choose **Create a new role**, and then choose Create role. This IAM role automatically gets permissions to access any S3 bucket that has sagemaker in the name. It gets these permissions through the AmazonSageMakerFullAccess policy, which SageMaker attaches to the role.
        - **Note**
    If you want to grant the IAM role permission to access S3 buckets without sagemaker
    in the name, you need to attach the S3FullAccess policy or limit the permissions
    to specific S3 buckets to the IAM role. For more information and examples of adding bucket policies to the IAM role, see Bucket Policy Examples.
    - d. Choose **Create notebook instance**. In a few minutes, SageMaker launches an ML compute instance—in this case, a notebook instance—and attaches a 5 GB of Amazon EBS storage volume to it. The notebook instance has a preconfigured Jupyter notebook server, SageMaker and AWS SDK libraries, and a set of Anaconda libraries. 
        
#### Step 4: Clean up
- Open the SageMaker console at https://console.aws.amazon.com/sagemaker/.
- Choose your Notebook instance, click **Actions** and click **stop**. while the status show stopped, click **Actions** and click **Delete**.

## Conclusion
✔Congratulations! Now you have learn how to create a Amazon SageMaker Notebook Instance. 

## Reference
https://docs.aws.amazon.com/sagemaker/latest/dg/sagemaker-dg.pdf



